import java.util.Comparator;

public class TimSort {

    static int MIN_MERGE = 32;

    public static <T extends Comparable<? super T>> void sort(T[] data) {
        // Sorts the data array of mutually comparable elements
        // by their natural order .
        sort(data, new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        });
    }

    /**
     * Get the minimum  length of subarray to be sorted inpendently
     *  
     * @param n minimum length of subarray
     * @return splitter length
     */
    private static  int miniRun(int n) {
        int r = 0;
        while (n >= MIN_MERGE)
        {
            r |= (n & 1);
            n >>= 1;
        }
        return n + r;
    }

    /**
     * This method sort the entire array using TimSort algorithm
     * 
     * @param   <T> type of the data
     * @param data  the array that needs to be sorted
     * @param cmp  comparator, to compare the element of the data
     */
    @SuppressWarnings("unchecked")
    public static <T>  void sort(T[] data,Comparator<? super T> cmp) {

        int minRun = miniRun(MIN_MERGE);
        int n = data.length;

        for (int i = 0; i < n; i += minRun) 
            insertionSort(data,cmp, i, Math.min((i + MIN_MERGE - 1), (n - 1)));

		for (int size = minRun; size < n; size = 2 * size) {

			for (int left = 0; left < n; left += 2 * size) {
                //Find the end of the left subarray,
                // mid+1 is the start of the right subarray
				int mid = left + size - 1;
				int right = Math.min((left + 2 * size - 1), (n - 1));

				if(mid < right)
                mergeSort(data,cmp,left, mid,right);		
            }
		}
    } 

    /**
     * Sorting data using insertion sort
     * 
     * @param <T> genetic type
     * @param data data to sort its sub array
     * @param cmp   comparator, to compare the element of the data
     * @param left  lower bound of sub array to be sorted
     * @param right  upped bound of sub array to be sorted
     */
    private static <T>  void insertionSort(T[] data, Comparator<? super T> cmp, int left, int right) {

        for (int i = left + 1; i <= right; i++) {
            T temp = data[i];
            int j = i - 1;
            while (j >= left && (cmp.compare(data[j], temp) > 0)) {
                data[j + 1] = data[j];
                j--;
            }
            data[j + 1] = temp;
        }
    }


    /**
     * Spliting two arrays to be merged in original array
     * 
     * @param <T> Genetic type
     * @param data  original array
     * @param cmp   comparator, to compare the element of the data
     * @param left  lower bound postion of subarray to be merged
     * @param mid   middle bound of subarray to be merged
     * @param right upperboud of subarray to be merged
     */
    @SuppressWarnings("unchecked")
    public static <T> void mergeSort(T[] data,  Comparator<? super T> cmp,  int left, int mid, int right) {
   	    // Original array is broken in two parts
		// left and right array
		int len1 = mid - left + 1, len2 = right - mid;
		int k = left;
		T[] l = (T[]) new Object[len1];
		T[] r = (T[]) new Object[len2];

		for (int x = 0; x < len1; x++) {
			l[x] = data[left + x];
		}
		for (int x = 0; x < len2; x++) {
			r[x] = data[mid + 1 + x];
		}
    
        merge(data,l, r, cmp, k);
    }

    /**
     * Merging two array to original array
     * 
     * @param <T>   generic type
     * @param data  original array
     * @param left  left array
     * @param right  right array
     * @param cmp   comparator, to compare the element of the data
     * @param k  start postion of left array
     */
    public static <T> void merge(T[] data, T[] left, T[] right,  Comparator<? super T> cmp, int k) {
    
        int i = 0, j = 0;

        while (i < left.length && j < right.length) {
            if (cmp.compare(left[i], right[j]) <= 0) {
                data[k++] = left[i++];
            } else {
                data[k++] = right[j++];	
            }
        }

        while (i < left.length) {
            data[k++] = left[i++];
        }

        while (j < right.length) {
            data[k++] = right[j++];
        }
    }
}