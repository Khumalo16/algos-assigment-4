

public final class Main {


    public static void main(String[] args) {   
        int l = 1000000;
        Integer[] arr = new Integer[l];
        int j = 0;

        Integer r = 0;
        for (int i = l-1; i >=0; i--) {
            r= (int) (Math.random() * l);
            arr[j++] = r;
            
        }


        TimSort ts = new TimSort();
        long start = System.currentTimeMillis();

        ts.sort(arr);
        long end = System.currentTimeMillis();
        System.out.println((end-start)/1000F);

    }
}

